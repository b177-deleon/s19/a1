// console.log(`Hello World!`)

const getCube = 7 ** 3
console.log(`The cube of 7 is ${getCube}`);

const address = ["1265", "Olen Thomas Drive", "Seymour", "Texas", '76380'];
const [houseNumber, street, city, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street} ${city}, ${state} ${zipCode}`);

const animal = {
	name: "Nellie",
	species: "dog",
	breed: "Border Collie",
	personality: "energetic",
	owner: "Nick Nelson"
}

const {name, species, breed, personality, owner} = animal;
console.log(`${name} is a female ${breed}. She is a very ${personality} type of ${species} owned by ${owner}.`);

const numbers = [2, 4, 6, 8, 10];
numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog("Marty", 5, "Shih tzu");
console.log(myDog);
